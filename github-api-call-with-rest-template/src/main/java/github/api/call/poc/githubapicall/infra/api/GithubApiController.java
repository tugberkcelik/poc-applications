package github.api.call.poc.githubapicall.infra.api;

import github.api.call.poc.githubapicall.domain.UserResponse;
import github.api.call.poc.githubapicall.infra.api.model.GetUserResponseRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class GithubApiController {

    @GetMapping("{userName}")
    public UserResponse getUserResponse(@PathVariable String userName)
    {
        GetUserResponseRequest getUserResponseRequest = new GetUserResponseRequest(userName);
        UserResponse userInfo = GithubHttpClientService.getUserInfo(getUserResponseRequest.getUserName());
        return userInfo;
    }
}


@Service
class GithubHttpClientService {

    private static RestTemplate restTemplate;

    @Autowired
    public GithubHttpClientService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public static UserResponse getUserInfo(String userName) {
        UserResponse userResponse = new UserResponse();

        String apiUrl = String.format("https://api.github.com/users/%s", userName);
        userResponse = restTemplate.getForObject(apiUrl, UserResponse.class); // isteği atıyor, response'ı deseri ediyor.

        return userResponse;
    }
}

package github.api.call.poc.githubapicall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubApiCallApplication {

    public static void main(String[] args) {
        SpringApplication.run(GithubApiCallApplication.class, args);
    }

}

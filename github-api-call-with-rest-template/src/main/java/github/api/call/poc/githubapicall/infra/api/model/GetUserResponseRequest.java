package github.api.call.poc.githubapicall.infra.api.model;

public class GetUserResponseRequest {
    private String userName;

    public GetUserResponseRequest(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

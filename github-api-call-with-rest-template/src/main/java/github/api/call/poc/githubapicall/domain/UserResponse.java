package github.api.call.poc.githubapicall.domain;

// @JsonIgnoreProperties(ignoreUnknown=true)
public class UserResponse { // json to model dto
    private Integer id;
    private Integer x;
    private String bio;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
